using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotador : MonoBehaviour
{
    public float rotationSpeed = 50f; // La velocidad de rotaci�n en grados por segundo

    // Update se llama una vez por frame
    void Update()
    {
        // Rota el objeto alrededor del eje Y
        transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
    }
}
