using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrojarPetardo : MonoBehaviour
{
    public GameObject Petardo;
    public KeyCode TeclaDePetardo = KeyCode.Q;
    public float FuerzaDeLanzamiento = 10f;
    public float tiempoDeVida = 3f;



    private void Update()
    {
        if (Input.GetKeyDown(TeclaDePetardo))
        {

            LanzarPetardo();


        }
    }

        private void LanzarPetardo()
        {
          if(Petardo !=null)
          {
            Vector3 DireccionLanzamiento = transform.forward;

            GameObject PetardoLanzado = Instantiate(Petardo, transform.position, Quaternion.identity);

            Rigidbody rb = PetardoLanzado.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddForce(DireccionLanzamiento * FuerzaDeLanzamiento, ForceMode.Impulse);
            }
            Destroy(PetardoLanzado, tiempoDeVida);
          }
        


        }
        
    
}
