using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarPoste : MonoBehaviour
{
    public Animator animator; // Referencia al Animator del objeto que contiene la animación que deseas activar
    public GameObject canvas2; // Referencia al objeto Canvas2

    private IEnumerator StartPosteCaeCoroutine()
    {
        // Esperar medio segundo
        yield return new WaitForSeconds(0.5f);

        // Activar la animación "PosteCae"
        animator.Play("PosteCae");

        // Esperar 0.7 segundos adicionales
        yield return new WaitForSeconds(0.6f);

        // Activar el objeto Canvas2
        canvas2.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            StartCoroutine(StartPosteCaeCoroutine());
        }
    }
}
