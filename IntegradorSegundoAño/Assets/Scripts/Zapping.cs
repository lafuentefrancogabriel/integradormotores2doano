using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zapping : MonoBehaviour
{
    public GameObject objectToDisappear;
    public GameObject objectToAppear;
    public GameObject thirdObject;

    private bool playerInInteractZone = false;

    private void Update()
    {
        if (playerInInteractZone && Input.GetKeyDown(KeyCode.E))
        {
            if (objectToDisappear != null && objectToDisappear.activeSelf)
            {
                objectToDisappear.SetActive(false);

                if (thirdObject != null)
                {
                    thirdObject.SetActive(true);
                }
            }
            else if (objectToAppear != null && !objectToAppear.activeSelf)
            {
                objectToAppear.SetActive(true);

                if (thirdObject != null)
                {
                    thirdObject.SetActive(false);
                }
            }
        }
    }

    // Cuando el jugador entra en la zona de interacción
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            playerInInteractZone = true;
        }
    }

    // Cuando el jugador sale de la zona de interacción
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            playerInInteractZone = false;
        }
    }
}
