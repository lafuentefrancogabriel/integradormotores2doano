using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float moveSpeed = 5f;  // Velocidad de movimiento del personaje
    public float mouseSensitivity = 2f;  // Sensibilidad del mouse
    public float jumpForce = 5f; // Fuerza del salto
    private bool isGrounded; // Variable para verificar si el personaje est� en el suelo

    private Rigidbody rb;
    private Camera characterCamera;
    private float cameraRotation = 0f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        characterCamera = Camera.main;

        // Bloquear y ocultar el cursor del mouse
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        // Obtener la entrada del teclado
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        // Calcular el vector de movimiento
        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);
        movement = transform.TransformDirection(movement) * moveSpeed;

        // Aplicar movimiento al Rigidbody del personaje
        rb.velocity = new Vector3(movement.x, rb.velocity.y, movement.z);

        // Si el personaje est� en el suelo y se presiona la tecla de salto (por ejemplo, la barra espaciadora)
        if (isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            // Aplicar fuerza vertical para simular el salto
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }

        // Obtener la entrada del mouse
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;

        // Rotar el personaje horizontalmente con la entrada del mouse
        transform.Rotate(Vector3.up, mouseX);

        // Rotar la c�mara verticalmente con la entrada del mouse
        cameraRotation -= mouseY;
        cameraRotation = Mathf.Clamp(cameraRotation, -90f, 90f);
        characterCamera.transform.localRotation = Quaternion.Euler(cameraRotation, 0f, 0f);
    }

    // Cuando el personaje entra en contacto con otro collider
    private void OnTriggerEnter(Collider other)
    {
        // Si el otro collider es el suelo
        if (other.CompareTag("Piso"))
        {
            isGrounded = true;
        }
    }

    // Cuando el personaje sale del contacto con otro collider
    private void OnTriggerExit(Collider other)
    {
        // Si el otro collider es el suelo
        if (other.CompareTag("Piso"))
        {
            isGrounded = false;
        }
    }
}