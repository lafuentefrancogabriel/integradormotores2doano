using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoAleatorio : MonoBehaviour
{
    public float velocidad = 5f;
    public float tiempoCambioDireccion = 2f;
    public float tiempoQuietoMin = 1f;
    public float tiempoQuietoMax = 3f;
    public float rangoMovimiento = 5f;
    public float alturaMaxima = 2f;

    public Transform jugador; // Referencia al jugador que el perro debe perseguir
    public float distanciaPersecucion = 10f; // Distancia a la que el perro detectar� al jugador y lo perseguir�
    public float velocidadPersecucion = 5f; // Velocidad a la que el perro persigue al jugador

    public GameObject petardo; // Referencia al petardo que puede arrojar el jugador
    public float distanciaAsustar = 5f; // Distancia a la que el petardo asusta al perro

    private float tiempoTranscurrido;
    private Vector3 direccion;
    private bool inicio = false;
    private bool asustado = false;

    void Start()
    {
        StartCoroutine(IniciarMovimientoAleatorio());
    }

    IEnumerator IniciarMovimientoAleatorio()
    {
        yield return new WaitForSeconds(0.1f);

        direccion = Random.insideUnitCircle.normalized;
        inicio = true;
    }

    void Update()
    {
        if (inicio && !asustado)
        {
            if (!PerroDetectaJugador())
            {
                if (tiempoTranscurrido >= tiempoCambioDireccion)
                {
                    if (Random.value < 0.2f) // Probabilidad de quedarse quieto (20%)
                    {
                        StartCoroutine(QuedarseQuieto(Random.Range(tiempoQuietoMin, tiempoQuietoMax)));
                    }
                    else
                    {
                        direccion = Vector3.Lerp(direccion, Random.insideUnitCircle.normalized, Time.deltaTime / tiempoCambioDireccion);
                    }

                    tiempoTranscurrido = 0f;
                }

                // Verificar si hay una pared en la direcci�n opuesta al jugador
                if (!VerificarColisionPared())
                {
                    transform.Translate(new Vector3(direccion.x, 0f, direccion.y) * velocidad * Time.deltaTime);
                }

                tiempoTranscurrido += Time.deltaTime;

                transform.position = new Vector3(
                    Mathf.Clamp(transform.position.x, -rangoMovimiento, rangoMovimiento),
                    Mathf.Clamp(transform.position.y, 0f, alturaMaxima),
                    Mathf.Clamp(transform.position.z, -rangoMovimiento, rangoMovimiento)
                );
            }
            else
            {
                // Perseguir al jugador
                Vector3 direccionPersecucion = (jugador.position - transform.position).normalized;
                transform.position += direccionPersecucion * velocidadPersecucion * Time.deltaTime;
            }
        }
    }

    IEnumerator QuedarseQuieto(float tiempo)
    {
        direccion = Vector3.zero; // Detiene el movimiento
        yield return new WaitForSeconds(tiempo);
        direccion = Random.insideUnitCircle.normalized; // Reanuda el movimiento aleatorio
    }

    bool PerroDetectaJugador()
    {
        if (jugador != null)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, jugador.position - transform.position, out hit, distanciaPersecucion))
            {
                if (hit.collider.CompareTag("Player"))
                {
                    return true;
                }
            }
        }
        return false;
    }

    bool VerificarColisionPared()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -direccion, out hit, 1f)) // Raycast en direcci�n opuesta al movimiento
        {
            if (!hit.collider.CompareTag("Player")) // Evitar que se detenga si choca con el jugador
            {
                return true; // Hay una pared en la direcci�n opuesta
            }
        }
        return false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Petardo"))
        {
            if (Vector3.Distance(transform.position, other.transform.position) <= distanciaAsustar)
            {
                asustado = true;
                StartCoroutine(ReiniciarPersecucion());
            }
        }
    }

    IEnumerator ReiniciarPersecucion()
    {
        yield return new WaitForSeconds(3f); // Tiempo de susto
        asustado = false;
        inicio = true; // Reanudar el movimiento aleatorio despu�s del susto
    }
}