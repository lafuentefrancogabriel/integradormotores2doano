using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMover : MonoBehaviour
{
    private Camera mainCamera;
    private bool isMoving = false;
    private Transform objectToMove;
    private Vector3 offset;
    private float maxRaycastDistance = 2f;
    private Transform playerTransform;
    private Rigidbody objectRigidbody; // Referencia al Rigidbody del objeto

    // Velocidad de interpolaci�n para suavizar el movimiento
    public float smoothSpeed = 5f;

    void Start()
    {
        mainCamera = Camera.main;
        playerTransform = GameObject.FindGameObjectWithTag("Jugador").transform;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !isMoving)
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, maxRaycastDistance))
            {
                if (hit.collider.CompareTag("Mobile"))
                {
                    isMoving = true;
                    objectToMove = hit.transform;
                    offset = objectToMove.position - hit.point;
                    objectRigidbody = objectToMove.GetComponent<Rigidbody>(); // Obtener el Rigidbody del objeto
                    if (objectRigidbody != null)
                    {
                        objectRigidbody.isKinematic = true; // Desactivar la f�sica del objeto mientras se mueve
                    }
                }
            }
        }

        if (objectToMove != null && isMoving && Input.GetMouseButton(0))
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, maxRaycastDistance))
            {
                Vector3 targetPosition = hit.point + offset;
                float distanceToPlayer = Vector3.Distance(targetPosition, playerTransform.position);

                if (distanceToPlayer > maxRaycastDistance)
                {
                    Vector3 directionToPlayer = (playerTransform.position - targetPosition).normalized;
                    targetPosition = playerTransform.position - directionToPlayer * maxRaycastDistance;
                }

                // Interpolaci�n suave hacia la nueva posici�n
                Vector3 smoothedPosition = Vector3.Lerp(objectToMove.position, targetPosition, smoothSpeed * Time.deltaTime);
                objectToMove.position = smoothedPosition;
            }
            else
            {
                // Si el rayo no impacta ninguna superficie, mantiene el objeto dentro de un l�mite
                Vector3 targetPosition = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, maxRaycastDistance));
                Vector3 smoothedPosition = Vector3.Lerp(objectToMove.position, targetPosition, smoothSpeed * Time.deltaTime);
                objectToMove.position = smoothedPosition;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (objectRigidbody != null)
            {
                objectRigidbody.isKinematic = false; // Activar la f�sica del objeto cuando se suelta
            }
            isMoving = false;
            objectToMove = null;
            objectRigidbody = null; // Limpiar la referencia al Rigidbody
        }
    }
}





