using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionVidrios : MonoBehaviour
{
   
    public Animator animator;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Pelota"))
        {
            animator.Play("VidriosRotos");
        }
    }
}

