using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadBobbing : MonoBehaviour
{
    public float velocidadBobbing = 0.18f; // Velocidad del movimiento del headbobbing
    public float cantidadBobbing = 0.2f; // Amplitud del movimiento del headbobbing
    public float puntoMedio = 2.0f; // Altura media de la cabeza del jugador

    private float temporizador = 0.0f;

    void Update()
    {
        // Obtener la entrada de movimiento del jugador (puede ser desde el teclado, joystick, etc.)
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (Mathf.Abs(horizontal) == 0 && Mathf.Abs(vertical) == 0)
        {
            temporizador = 0.0f;
        }
        else
        {
            // Calcular el movimiento de headbobbing
            float sliceOnda = 0.0f;
            float movimientoHorizontal = Mathf.Abs(horizontal);
            float movimientoVertical = Mathf.Abs(vertical);

            if (movimientoHorizontal == 0 && movimientoVertical == 0)
            {
                sliceOnda = 0.0f;
                temporizador = 0.0f;
            }
            else
            {
                sliceOnda = Mathf.Sin(temporizador);
                temporizador = temporizador + velocidadBobbing;
                if (temporizador > Mathf.PI * 2)
                {
                    temporizador = temporizador - (Mathf.PI * 2);
                }
            }

            // Aplicar el movimiento de headbobbing a la posici�n de la c�mara
            float cambioTraslacion = sliceOnda * cantidadBobbing;
            float ejesTotales = Mathf.Abs(movimientoHorizontal) + Mathf.Abs(movimientoVertical);
            ejesTotales = Mathf.Clamp(ejesTotales, 0.0f, 1.0f);
            cambioTraslacion = ejesTotales * cambioTraslacion;
            Vector3 posicionLocal = transform.localPosition;
            posicionLocal.y = puntoMedio + cambioTraslacion;
            transform.localPosition = posicionLocal;
        }
    }
}