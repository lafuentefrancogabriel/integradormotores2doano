using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AplicarFuerza : MonoBehaviour
{
    public Rigidbody pelota;
    public float fuerza = 10f;
    public float rotacion = 100f;
    public float elevacion = 2f;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            Vector3 direccion = collision.transform.forward;
            pelota.AddForce(direccion * fuerza, ForceMode.Impulse);
            pelota.AddTorque(new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * rotacion, ForceMode.Impulse);
            pelota.AddForce(Vector3.up * elevacion, ForceMode.Impulse);
        }
    }
}
