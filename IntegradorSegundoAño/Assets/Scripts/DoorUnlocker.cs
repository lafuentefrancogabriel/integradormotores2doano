using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorUnlocker : MonoBehaviour
{
    public Animator cajonAnimator; // Referencia al Animator del objeto con la animaci�n "CajonAbre"

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Mobile"))
        {
            GameObject cerradura = GameObject.FindGameObjectWithTag("Cerrojo");
            if (cerradura != null)
            {
                Destroy(cerradura);

                // Activa la animaci�n "CajonAbre"
                if (cajonAnimator != null)
                {
                    cajonAnimator.enabled = true; // Activa el Animator si no est� activado
                    cajonAnimator.Play("CajonAbre"); // Reproduce la animaci�n directamente
                }
            }
        }
    }
}

