using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaPatio : MonoBehaviour
{
    public Animator animator;
    public string tagActivadorAnimacion = "ActivadorAnimacion";
    public float tiempoEspera = 6f;
    private bool enContacto;
    private bool primeraAnimacionActivada;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            enContacto = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            enContacto = false;
        }
    }

    private void Update()
    {
        if (enContacto && Input.GetKeyDown(KeyCode.E))
        {
            if (!primeraAnimacionActivada)
            {
                animator.Play("PatioPuerta");
                primeraAnimacionActivada = true;
                StartCoroutine(ActivarSegundaAnimacion());
            }
        }
        else if (enContacto && Input.GetKeyDown(KeyCode.C))
        {
            if (primeraAnimacionActivada)
            {
                animator.Play("PatioPuertaClose");
                primeraAnimacionActivada = false;
            }
        }
    }

    private IEnumerator ActivarSegundaAnimacion()
    {
        yield return new WaitForSeconds(tiempoEspera);
        animator.Play("PatioPuertaClose");
        primeraAnimacionActivada = false;
    }
}
