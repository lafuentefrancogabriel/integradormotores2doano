using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventario : MonoBehaviour
{
    private GameObject fulboInstance;  // Referencia al objeto Fulbo actual
    public float distanciaAparecerFulbo = 1.0f; // Distancia a la que aparecer� el Fulbo adelante del jugador

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (fulboInstance == null)  // Si no se ha recogido el Fulbo, se busca uno cerca del jugador
            {
                BuscarFulboCerca();
            }
            else  // Si ya se recogi� el Fulbo, se hace aparecer en la posici�n del jugador
            {
                HacerAparecerFulbo();
            }
        }
    }

    void BuscarFulboCerca()
    {
        // Encuentra todos los colliders del jugador en un radio determinado
        Collider[] colliders = Physics.OverlapSphere(transform.position, 2f);
        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Pelota"))
            {
                fulboInstance = collider.gameObject;
                fulboInstance.SetActive(false);  // Desactiva el objeto Fulbo
                break;
            }
        }
    }

    void HacerAparecerFulbo()
    {
        if (fulboInstance != null)
        {
            // Calcula la posici�n adelante del jugador y coloca el Fulbo en esa posici�n
            Vector3 posicionAdelante = transform.position + transform.forward * distanciaAparecerFulbo;
            fulboInstance.transform.position = posicionAdelante;

            // Restablece la velocidad de la pelota a cero para detener su movimiento
            Rigidbody rigidbody = fulboInstance.GetComponent<Rigidbody>();
            if (rigidbody != null)
            {
                rigidbody.velocity = Vector3.zero;
            }

            fulboInstance.SetActive(true);  // Activa el objeto Fulbo
            fulboInstance = null;  // Restablece la referencia a null
        }
    }
}
