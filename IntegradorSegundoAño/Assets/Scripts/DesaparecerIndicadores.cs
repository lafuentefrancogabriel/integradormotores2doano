using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesaparecerIndicadores : MonoBehaviour
{
    public GameObject objetoADesaparecer;

    private void OnTriggerStay(Collider other)
    {
        // Verificar si el collider de "Jugador" est� en contacto con el objeto actual
        if (other.CompareTag("Jugador") && Input.GetKeyDown(KeyCode.E))
        {
            // Hacer que el objeto desaparezca
            objetoADesaparecer.SetActive(false);
        }
    }
}
