using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApagarLucesColision : MonoBehaviour
{
    public string jugadorTag = "Jugador";
    public Light[] lucesAapagar;
    public int vecesAapagar = 5;
    public float tiempoEncendido = 2f; // Tiempo en segundos antes de que las luces se vuelvan a encender

    private int vecesApagadas = 0;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(jugadorTag) && vecesApagadas < vecesAapagar)
        {
            // El objeto "Jugador" ha entrado en el collider de "apagarluces"
            // Apagamos las luces
            foreach (Light luz in lucesAapagar)
            {
                luz.enabled = false;
            }

            vecesApagadas++;

            if (vecesApagadas >= vecesAapagar)
            {
                Debug.Log("Se han apagado las luces " + vecesAapagar + " veces.");
            }
            else
            {
                // Iniciamos una corutina para volver a encender las luces despu�s de un tiempo
                StartCoroutine(EncenderLucesDespuesDeEspera());
            }
        }
    }

    private System.Collections.IEnumerator EncenderLucesDespuesDeEspera()
    {
        yield return new WaitForSeconds(tiempoEncendido);

        // Encendemos las luces despu�s del tiempo de espera
        foreach (Light luz in lucesAapagar)
        {
            luz.enabled = true;
        }
    }
}