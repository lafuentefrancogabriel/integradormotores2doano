using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerroAsustadizo : MonoBehaviour
{
    public float velocidadAlejarse = 5f;
    public float distanciaMinima = 5f;
    public float duracionAsustado = 3f;
    public Material materialNormal;
    public Material materialAsustado;

    private bool asustado = false;
    private float tiempoAsustado = 0f;
    private Renderer renderer;

    void Start()
    {
        renderer = GetComponent<Renderer>();
        renderer.material = materialNormal;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Petardo"))
        {
            asustado = true;
            tiempoAsustado = 0f;
            Vector3 direccionAlejarse = transform.position - other.transform.position;
            direccionAlejarse.y = 0;
            direccionAlejarse.Normalize();
            StartCoroutine(AlejarsePorTiempo(direccionAlejarse, duracionAsustado));
            renderer.material = materialAsustado;
        }
    }

    void Update()
    {
        if (asustado)
        {
            tiempoAsustado += Time.deltaTime;

            if (tiempoAsustado >= duracionAsustado)
            {
                asustado = false;
                tiempoAsustado = 0f;
                renderer.material = materialNormal;
            }
        }
    }

    private IEnumerator AlejarsePorTiempo(Vector3 direccion, float duracion)
    {
        float timer = 0f;
        while (timer < duracion)
        {
            transform.position += direccion * velocidadAlejarse * Time.deltaTime;
            timer += Time.deltaTime;
            yield return null;
        }
    }
}
/*Codigo viejo
public class PerroAsustadizo : MonoBehaviour
{
    public float velocidadAlejarse = 5f;
    public float distanciaMinima = 5f;
    public float duracionAsustado = 3f;
    public GameObject colliderPetardoPrefab;
    public Material materialNormal;
    public Material materialAsustado;

    private bool asustado = false;
    private float tiempoAsustado = 0f;
    private new Renderer renderer;

    void Start()
    {
        // Obtener el componente Renderer del perro
        renderer = GetComponent<Renderer>();

        // Asignar el material normal al inicio
        renderer.material = materialNormal;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Petardo"))
        {
            Debug.Log("�El perro ha detectado el petardo!");
            asustado = true;
            tiempoAsustado = 0f;
            Vector3 direccionAlejarse = transform.position - other.transform.position;
            direccionAlejarse.y = 0;
            direccionAlejarse.Normalize();
            direccionAlejarse *= velocidadAlejarse;
            transform.position += direccionAlejarse * Time.deltaTime;

            // Cambiar al material de asustado
            renderer.material = materialAsustado;
        }
    }

    void Update()
    {
        if (asustado)
        {
            Debug.Log("El perro est� asustado por el petardo.");

            tiempoAsustado += Time.deltaTime;

            if (tiempoAsustado >= duracionAsustado)
            {
                Debug.Log("El perro ha dejado de estar asustado.");
                asustado = false;
                tiempoAsustado = 0f;

                // Volver al material normal
                renderer.material = materialNormal;
            }
        }
    }

    public void CrearColliderPetardo(float tiempo)
    {
        Debug.Log("Creando collider del petardo...");
        Invoke("ActivarColliderPetardo", tiempo);
    }

    void ActivarColliderPetardo()
    {
        if (colliderPetardoPrefab != null)
        {
            Debug.Log("Collider del petardo activado.");
            Instantiate(colliderPetardoPrefab, transform.position, Quaternion.identity);
        }
    }
}
 
 */