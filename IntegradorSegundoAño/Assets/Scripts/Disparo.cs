using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    public GameObject proyectilPrefab;
    public Transform puntoDeDisparo;
    public float fuerzaMinima = 10f;
    public float fuerzaMaxima = 50f;
    public float tiempoMaximo = 2f;
    public float tiempoDeVidaProyectil = 5f;
    

    private float tiempoDeDisparo;

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            tiempoDeDisparo = Time.time;
        }
        else if (Input.GetMouseButtonUp(1))
        {
            Disparar();
        }
    }

    void Disparar()
    {
        float tiempoDePresion = Time.time - tiempoDeDisparo;
        float fuerzaDelDisparo = Mathf.Clamp(tiempoDePresion / tiempoMaximo, 0f, 1f) * (fuerzaMaxima - fuerzaMinima) + fuerzaMinima;

        GameObject proyectil = Instantiate(proyectilPrefab, puntoDeDisparo.position, puntoDeDisparo.rotation);
        Rigidbody rb = proyectil.GetComponent<Rigidbody>();
        rb.AddForce(puntoDeDisparo.forward * fuerzaDelDisparo, ForceMode.Impulse);

        Destroy(proyectil, tiempoDeVidaProyectil);
    }
    
}