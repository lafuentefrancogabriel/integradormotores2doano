using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPersecucion : MonoBehaviour
{
    public Transform jugador; // Referencia al jugador que el perro debe perseguir
    public float distanciaPersecucion = 10f; // Distancia a la que el perro detectar� al jugador y lo perseguir�
    public float velocidadPersecucion = 5f; // Velocidad a la que el perro persigue al jugador

    private void Update()
    {
        if (enabled) // Solo ejecutar el movimiento si este componente est� activo
        {
            // Calcular la distancia entre el perro y el jugador
            float distanciaAlJugador = Vector3.Distance(transform.position, jugador.position);

            // Si el jugador est� dentro de la distancia de persecuci�n
            if (distanciaAlJugador <= distanciaPersecucion)
            {
                // Perseguir al jugador
                Vector3 direccionPersecucion = (jugador.position - transform.position).normalized;
                transform.position += direccionPersecucion * velocidadPersecucion * Time.deltaTime;
            }
        }
    }
}

