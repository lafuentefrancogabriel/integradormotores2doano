using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialogo : MonoBehaviour
{
    [SerializeField] private GameObject PanelDialogos;
    [SerializeField] private TMP_Text TextoDialogo;
    [SerializeField, TextArea(4, 6)] private string[] dialogueLines;
    private bool JugadorEnRango;
    private bool inicioElDialogo;
    private int lineaMostrada;
    private float TiempoDeEscritura = 0.05f;
    private bool mostrandoLinea;

    void Update()
    {
        if (JugadorEnRango && Input.GetKeyDown("e"))
        {
            if (!inicioElDialogo)
            {
                GestorDeAudio.instancia.reproducir("Murmullo");
                StartDialogue();
            }
            else if (!mostrandoLinea)
            {
                SiguienteLinea();
            }
        }
    }

    private void StartDialogue()
    {
        inicioElDialogo = true;
        PanelDialogos.SetActive(true);
        lineaMostrada = 0;
        mostrandoLinea = false;
        StartCoroutine(ShowLine());
    }

    private void SiguienteLinea()
    {
        lineaMostrada++;
        if (lineaMostrada < dialogueLines.Length)
        {
            StartCoroutine(ShowLine());
        }
        else
        {
            inicioElDialogo = false;
            PanelDialogos.SetActive(false);
        }
    }

    private IEnumerator ShowLine()
    {
        mostrandoLinea = true;
        TextoDialogo.text = string.Empty;

        foreach (char ch in dialogueLines[lineaMostrada])
        {
            TextoDialogo.text += ch;
            yield return new WaitForSeconds(TiempoDeEscritura);
        }

        mostrandoLinea = false;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            JugadorEnRango = true;
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            JugadorEnRango = false;
        }
    }
}
