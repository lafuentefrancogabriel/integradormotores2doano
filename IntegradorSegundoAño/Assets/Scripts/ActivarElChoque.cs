using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarElChoque : MonoBehaviour
{
    public Animator animator; // Referencia al Animator del objeto que contiene la animación que deseas activar

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            animator.Play("Patrulla"); // Activar la animación utilizando un disparador llamado "ActivarAnimacion"
        }
    }
}
