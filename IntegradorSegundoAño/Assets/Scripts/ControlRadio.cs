using UnityEngine;

public class ControlRadio : MonoBehaviour
{
    public GameObject radioSonido;
    public KeyCode activationKey = KeyCode.E;

    private bool isPlayerInside = false;
    private bool isRadioActive = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            isPlayerInside = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            isPlayerInside = false;
            radioSonido.SetActive(false);
            isRadioActive = false;
        }
    }

    private void Update()
    {
        if (isPlayerInside && Input.GetKeyDown(activationKey))
        {
            if (isRadioActive)
            {
                radioSonido.SetActive(false);
                isRadioActive = false;
            }
            else
            {
                radioSonido.SetActive(true);
                isRadioActive = true;
            }
        }
    }
}
